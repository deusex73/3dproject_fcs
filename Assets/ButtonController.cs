﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {
	private bool _isActive = true;

	[SerializeField]
	private ObjectController _subject;

	[SerializeField]
	private string _param;

	[SerializeField]
	private bool _repeatable = false;

	[SerializeField]
	private string _requireItem;

	[SerializeField]
	private int _requireValue = 1;

	[SerializeField]
	private bool _takeRequire = false;

	public void OnClick() {
		if (_isActive && _subject != null && !string.IsNullOrEmpty (_param)) {
			if (string.IsNullOrEmpty (_requireItem) || PortfolioManager.Instance.Check (_requireItem, _requireValue)) {
				if (_takeRequire) {
					PortfolioManager.Instance.Remove (_requireItem, _requireValue);
				}
				_subject.Activate (_param);
				_isActive = _repeatable;
			}
		}
	}
}
