﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainCamera : MonoBehaviour {
	private const float MOUSE_ROTATION_SPEED = 2f;
	private const float MOUSE_SCROLL_SPEED = 0.5f;

	[SerializeField]
	private GameObject _Avatar;

	[SerializeField]
	private Camera _camera;

	private void MouseRotation() {
		if (Input.GetMouseButtonDown (1)) {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		if (Input.GetMouseButton(1)) {
			transform.Rotate(transform.up, Input.GetAxis("Mouse X") * MOUSE_ROTATION_SPEED);
		}
		if (Input.GetMouseButtonUp (1)) {
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
	}

	private void MouseScroll() {
		if (_camera != null) {
			Vector3 position = _camera.transform.localPosition;
			position.z += Input.mouseScrollDelta.y;
			position.z = Mathf.Clamp (position.z, -10f, -4f);
			position.y = position.z * -0.5f - 2f;
			_camera.transform.localPosition = position;

			Vector3 rotation = _camera.transform.localEulerAngles;
			rotation.x = position.z * -5f - 30f;
			_camera.transform.localEulerAngles = rotation;
		}
	}

	private void Update() {
		if (_Avatar != null) {
			transform.position = _Avatar.transform.position;
		}
		MouseRotation ();
		MouseScroll ();
	}
}
