﻿using UnityEngine;
using System.Collections;

public class PickupItem : MonoBehaviour {
	[SerializeField]
	public string Item;

	[SerializeField]
	public int Value = 1;
}
