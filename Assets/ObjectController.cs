﻿using UnityEngine;
using System.Collections;

public class ObjectController : MonoBehaviour {
	[SerializeField]
	private Animator _animation;

	public void Activate(string obj) {
		if (_animation != null) {
			_animation.SetTrigger (obj);
		}
	}
}
