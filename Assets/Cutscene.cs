﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cutscene : MonoBehaviour {
	[SerializeField]
	private List<RectTransform> _pages = new List<RectTransform>();

	private int _currentPage;
	private bool _played = false;

	private void Awake () {
		foreach (RectTransform page in _pages) {
			page.gameObject.SetActive (false);
		}
		_currentPage = 0;
		gameObject.SetActive (false);
	}

	public void ShowFirstPage() {
		if (_played)
			return;
		gameObject.SetActive (true);
		_currentPage = 0;
		Time.timeScale = 0;
		ShowNextPage ();
	}

	public void ShowNextPage() {
		if (_currentPage > 0) {
			_pages [_currentPage - 1].gameObject.SetActive (false);
		}
		if (_currentPage < _pages.Count) {
			_pages [_currentPage].gameObject.SetActive (true);
			_currentPage++;
		} else {
			gameObject.SetActive (false);
			_played = true;
			Time.timeScale = 1f;
		}
	}
}
