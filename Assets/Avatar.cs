﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.ThirdPerson;

public class Avatar : MonoBehaviour {
	private Vector3 _targetPosition;
	private Vector3 _uncomfirmedPosition;

	public static Avatar Instance;

	private void Awake() {
		Instance = this;
	}

	public void SetupTargetPosition(Vector3 newTargetPosition) {
		_targetPosition = newTargetPosition;
		ThirdPersonUserControl charController = GetComponent<ThirdPersonUserControl> ();
		charController.enabled = newTargetPosition == Vector3.zero;
		_uncomfirmedPosition = Vector3.zero;
	}

	private void CheckInput() {
		if (Input.GetMouseButtonDown (0)) {
			_uncomfirmedPosition = Input.mousePosition;
		} else if (Input.GetMouseButtonUp (0)) {
			if (_uncomfirmedPosition == Vector3.zero)
				return;
			Vector3 currentPosition = Input.mousePosition;
			if (Vector3.Distance (_uncomfirmedPosition, currentPosition) < 1f) {
				Ray ray = Camera.main.ScreenPointToRay (_uncomfirmedPosition);
				RaycastHit[] hits = Physics.RaycastAll (ray);
				if (hits.Length == 0)
					return;
				
				RaycastHit nearestHit = new RaycastHit();
				nearestHit.distance = Mathf.Infinity;
				foreach (RaycastHit current in hits) {
					if (current.distance < nearestHit.distance) {
						nearestHit = current;
					}
				}

				SetupTargetPosition (nearestHit.point);
			}
        } else if (Input.GetMouseButtonDown(1)) {

		} else if (Input.anyKey && !Input.GetMouseButton(0)) {
			SetupTargetPosition (Vector3.zero);
		}
	}

	private void MoveToTarget() {
		ThirdPersonCharacter character = GetComponent<ThirdPersonCharacter> ();
		Vector3 direction = _targetPosition - transform.position;
		direction.y = 0f;
		if (direction.magnitude > 0.5f) {
			character.Move (direction, false, false);
		} else {
			SetupTargetPosition (Vector3.zero);
		}
	}

	private void FixedUpdate() {
		if (Time.timeScale == 0f)
			return;

		CheckInput ();
		if (_targetPosition != Vector3.zero) {
			MoveToTarget ();
		}
	}

	private void OnTriggerEnter(Collider other) {
		PickupItem pickup = other.GetComponent<PickupItem> ();
		if (pickup != null) {
			PortfolioManager.Instance.Add (pickup.Item, pickup.Value);
		}

		ButtonController button = other.GetComponent<ButtonController> ();
		if (button != null) {
			button.OnClick ();
		}

		CutsceneLauncher cutsceneLauncher = other.GetComponent<CutsceneLauncher> ();
		if (cutsceneLauncher != null) {
			cutsceneLauncher.Cutscene.ShowFirstPage ();
		}

        if (other.tag == "HitBox") {
            Mob mob = other.GetComponentInParent<Mob>();
            if (mob != null) {
                mob.Kill();
            }
        }

		if (other.tag == "Target") {
			Destroy (other.gameObject);
		}
		if (other.tag == "DeathObject") {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
	}
}