﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PortfolioManager : MonoBehaviour {
	[SerializeField]
	private Text _text;

	private Dictionary<string, int> Items = new Dictionary<string, int> ();

	public static PortfolioManager Instance;

	private void Awake() {
		Instance = this;
	}

	private void UpdateText() {
		if (_text == null)
			return;
		_text.text = "Инвентарь\n";
		foreach (KeyValuePair<string, int> item in Items) {
			_text.text += item.Key + " : " + item.Value + "\n";
		}
	}

	public void Add(string item, int value) {
		if (Items.ContainsKey (item)) {
			Items [item] += value;
		} else {
			Items.Add (item, value);
		}
		UpdateText ();
	}

	public bool Remove(string item, int value) {
		if (Check (item, value)) {
			Items [item] -= value;
			if (Items [item] == 0) {
				Items.Remove (item);
			}
			UpdateText ();
			return true;
		} else {
			return false;
		}
	}

	public bool Check(string item, int value) {
		return Items.ContainsKey (item) && Items [item] >= value;
	}
}
