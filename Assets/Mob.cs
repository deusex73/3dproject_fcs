﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Ball;

public class Mob : MonoBehaviour {
    [SerializeField]
    private Ball _ballComponent;

    [SerializeField]
    private GameObject _sensors;

    [SerializeField]
    private Animator _animation;

    public void Kill() {
        Collider[] colliders = GetComponentsInChildren<Collider>();
        foreach (Collider item in colliders) {
            if (item.isTrigger) {
                item.enabled = false;
            }
        }
        _animation.SetTrigger("Death");
    }

    private void DestroyObject() {
        Destroy(gameObject);
    }

    private void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            _ballComponent.Move(other.transform.position - transform.position, false);
        }
    }

    private void FixedUpdate() {
        _sensors.transform.localRotation = Quaternion.Inverse(_ballComponent.transform.localRotation);
    }
}
